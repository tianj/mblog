<textarea id="comment" name="comment" rows="2" class="form-control" required></textarea>

<script type="text/javascript">
/*seajs.use('editor', function(editor) {
	editor.init(function () {
		$('#comment').removeClass('form-control');
	});
});*/
</script>

<script type="text/javascript" charset="utf-8" src="${base}/dist/js/jquery.form.min.js"></script>
<script type="text/javascript" charset="utf-8" src="${base}/dist/vendors/tinymce_v5.4.1/tinymce.min.js"></script>

<script type="text/javascript">
    $(function () {

        tinymce.init({
            selector: "#comment",
            language: "zh_CN",
            height: 150,
            plugins: [
                'emoticons'
            ],
            toolbar: "emoticons ",
            menubar: false,
            statusbar : false,
            paste_data_images: true,
            convert_urls: false,
            body_class: 'markdown-body',
            setup: function(editor) {
                /*editor.on('change', function(e) {
                    tinymce.triggerSave();
                    $("#" + editor.id).valid();
                });*/
            }
        });
    })
</script>

