package com.mtons.mblog.modules.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 评论的点赞情况
 * @author shenzz on 2020.07.29
 */
@Entity
@Table(name = "mto_favorite_comment", indexes = {
        @Index(name = "IK_USER_ID", columnList = "user_id")
})
public class FavoriteComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 所属用户
     */
    @Column(name = "user_id")
    private long userId;

    /**
     * 内容ID
     */
    @Column(name = "post_id")
    private long postId;

    /**
     * 评论的ID
     */
    @Column(name = "comment_id")
    private long commentId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }
}
