/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package com.mtons.mblog.modules.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * 评论
 *
 * @author langhsu
 */
@Entity
@Table(name = "mto_comment", indexes = {
        @Index(name = "IK_POST_ID", columnList = "post_id")
})
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 父评论ID
     */
    private long pid;

    /**
     * 所属内容ID
     */
    @Column(name = "post_id")
    private long postId;

    /**
     * 评论内容
     */
    @Column(name = "content")
    private String content;

    @Column(name = "created")
    private Date created;

    @Column(name = "author_id")
    private long authorId;

    private int status;

    /**
     * ip
     */
    @Column(name = "ip")
    private String ip;

    /**
     * favors
     * 点赞数
     */
    @Column(name = "favors")
    private int favors;

    /**
     * 评论点赞情况
     * 表示数据库中不存在
     */
    @Transient
    private String favorStatus;

    /**
     * 审核情况：审核中、审核通过、审核不通过、无需审核
     */
    @Column(name = "check_text")
    private String checkText;

    /**
     * 删除时间
     */
    @Column(name = "del_time")
    private Date delTime;

    /**
     * 操作人ID
     */
    @Column(name = "del_user_id")
    private int delUserId;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPid() {
        return pid;
    }

    public void setPid(long pid) {
        this.pid = pid;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getFavors() {
        return favors;
    }

    public void setFavors(int favors) {
        this.favors = favors;
    }

    public String getFavorStatus() {
        return favorStatus;
    }

    public void setFavorStatus(String favorStatus) {
        this.favorStatus = favorStatus;
    }

    public String getCheckText() {
        return checkText;
    }

    public void setCheckText(String checkText) {
        this.checkText = checkText;
    }

    public Date getDelTime() {
        return delTime;
    }

    public void setDelTime(Date delTime) {
        this.delTime = delTime;
    }

    public int getDelUserId() {
        return delUserId;
    }

    public void setDelUserId(int delUserId) {
        this.delUserId = delUserId;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", pid=" + pid +
                ", postId=" + postId +
                ", content='" + content + '\'' +
                ", created=" + created +
                ", authorId=" + authorId +
                ", status=" + status +
                ", ip='" + ip + '\'' +
                ", favors=" + favors +
                ", favorStatus='" + favorStatus + '\'' +
                ", checkText='" + checkText + '\'' +
                ", delTime=" + delTime +
                ", delUserId=" + delUserId +
                '}';
    }
}
