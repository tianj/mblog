package com.mtons.mblog.modules.repository;

import com.mtons.mblog.modules.entity.Favorite;
import com.mtons.mblog.modules.entity.FavoriteComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author langhsu
 */
public interface FavoriteCommentRepository extends JpaRepository<FavoriteComment, Long>, JpaSpecificationExecutor<FavoriteComment> {

    FavoriteComment findByUserIdAndCommentId(Long userId, Long commentId);
}
