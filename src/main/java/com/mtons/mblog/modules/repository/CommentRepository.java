/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package com.mtons.mblog.modules.repository;

import com.mtons.mblog.modules.entity.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author langhsu
 */
public interface CommentRepository extends JpaRepository<Comment, Long>, JpaSpecificationExecutor<Comment> {

	Page<Comment> findAll(Pageable pageable);
	/**
	 * 获取delTime为null的Comment
	 */
	Page<Comment> findAllByDelTimeNull(Pageable pageable);
	Page<Comment> findAllByDelTimeNullAndCheckTextIn(Pageable pageable, Collection<String> checkTextArr);



	Page<Comment> findAllByPostId(Pageable pageable, long postId);
	Page<Comment> findAllByAuthorId(Pageable pageable, long authorId);
	/**
	 * @param pageable
	 * @param postId
	 * @return
	 */
	Page<Comment> findAllByPostIdAndDelTimeNull(Pageable pageable, long postId);

	List<Comment> findAllByPostIdAndDelTimeNullAndCheckTextIn(long postId, Collection<String> checkTextArr);

	Page<Comment> findAllByPostIdAndDelTimeNullAndCheckTextIn(Pageable pageable, long postId, Collection<String> checkTextArr);
	/**
	 * @param pageable
	 * @param authorId
	 * @return
	 */
	Page<Comment> findAllByAuthorIdAndDelTimeNull(Pageable pageable, long authorId);

	List<Comment> removeByIdIn(Collection<Long> ids);
	/**
	 * 批量设置delTime值
	 */
	@Modifying
	@Query


	List<Comment> removeByPostId(long postId);
	/**
	 * 根据postId获取所有评论
	 * @param postId
	 * @return
	 */
	List<Comment> queryAllByPostId(long postId);

	long countByAuthorIdAndPostId(long authorId, long postId);

	@Modifying
	@Query("update Comment set favors = favors + :increment where id = :id")
	void updateFavors(@Param("id") long id, @Param("increment") int increment);

	@Modifying
	@Query("update Comment set delTime = :date where id = :id")
	void updateDelTime(@Param("id") long id, Date date);

	@Modifying
	@Query("update Comment set checkText = :checkText where id = :id")
	void updateCheck(@Param("id") long id, @Param("checkText") String checkText);
}
