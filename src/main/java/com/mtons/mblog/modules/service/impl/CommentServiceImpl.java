/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package com.mtons.mblog.modules.service.impl;

import com.mtons.mblog.base.lang.Consts;
import com.mtons.mblog.modules.data.CommentVO;
import com.mtons.mblog.modules.entity.Comment;
import com.mtons.mblog.modules.entity.FavoriteComment;
import com.mtons.mblog.modules.event.MessageEvent;
import com.mtons.mblog.modules.repository.CommentRepository;
import com.mtons.mblog.modules.repository.FavoriteCommentRepository;
import com.mtons.mblog.modules.service.*;
import com.mtons.mblog.modules.service.complementor.CommentComplementor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author langhsu
 *
 */
@Service
@Transactional(readOnly = true)
public class CommentServiceImpl implements CommentService {
	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private FavoriteCommentRepository favoriteCommentRepository;
	@Autowired
	private UserService userService;
	@Autowired
	private UserEventService userEventService;
	@Autowired
	private PostService postService;
	@Autowired
	private FavoriteService favoriteService;

	static String[] checkTextArr = {"审核通过", "无需审核"};

	@Override
	public Page<CommentVO> paging4Admin(Pageable pageable) {
		Page<Comment> page = commentRepository.findAllByDelTimeNull(pageable);
		List<CommentVO> rets = CommentComplementor.of(page.getContent())
				.flutBuildUser()
				.flutBuildPost()
				.getComments();
		return new PageImpl<>(rets, pageable, page.getTotalElements());
	}

	@Override
	public Page<CommentVO> pagingByAuthorId(Pageable pageable, long authorId) {
		// 只取delTime为null的值
		// Page<Comment> page = commentRepository.findAllByAuthorId(pageable, authorId);
		Page<Comment> page = commentRepository.findAllByAuthorIdAndDelTimeNull(pageable, authorId);

		List<CommentVO> rets = CommentComplementor.of(page.getContent())
				.flutBuildUser()
				.flutBuildParent()
				.flutBuildPost()
				.getComments();
		return new PageImpl<>(rets, pageable, page.getTotalElements());
	}

	@Override
	public Page<CommentVO> pagingByPostId(Pageable pageable, long postId, long userId) {
		// Page<Comment> page = commentRepository.findAllByPostId(pageable, postId);
		// Page<Comment> page = commentRepository.findAllByPostIdAndDelTimeNull(pageable, postId);
		// TODO: 确认返回值内容！！！
		Page<Comment> page =  commentRepository.findAllByPostIdAndDelTimeNullAndCheckTextIn(pageable, postId, Arrays.asList(checkTextArr));
		List<CommentVO> rets = CommentComplementor.of(page.getContent())
				.flutBuildUser()
				.flutBuildParent()
				.getComments();
		rets.forEach(commentVO -> {
			FavoriteComment fc = favoriteCommentRepository.findByUserIdAndCommentId(userId, commentVO.getId());
			if(fc == null){
				commentVO.setFavorStatus("赞");
			}else{
				commentVO.setFavorStatus("已赞");
			}
		});
		return new PageImpl<>(rets, pageable, page.getTotalElements());
	}

	@Override
	public List<CommentVO> findLatestComments(int maxResults) {
		Pageable pageable = PageRequest.of(0, maxResults, Sort.by(Sort.Direction.DESC, "id"));
		// 获取delTime不为NULL的值
		// Page<Comment> page = commentRepository.findAll(pageable);
		//Page<Comment> page = commentRepository.findAllByDelTimeNull(pageable);
		Page<Comment> page = commentRepository.findAllByDelTimeNullAndCheckTextIn(pageable, Arrays.asList(checkTextArr));
		return CommentComplementor.of(page.getContent())
				.flutBuildUser()
				.getComments();
	}

	@Override
	public Map<Long, CommentVO> findByIds(Set<Long> ids) {
		List<Comment> list = commentRepository.findAllById(ids);
		return CommentComplementor.of(list)
				.flutBuildUser()
				.toMap();
	}

	@Override
	public Comment findById(long id) {
		return commentRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public long post(CommentVO comment) {
		Comment po = new Comment();
		
		po.setAuthorId(comment.getAuthorId());
		po.setPostId(comment.getPostId());
		po.setContent(comment.getContent());
		po.setCreated(new Date());
		po.setPid(comment.getPid());
		po.setIp(comment.getIp());
		po.setCheckText(comment.getCheckText());
		commentRepository.save(po);

		userEventService.identityComment(comment.getAuthorId(), true);
		return po.getId();
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void delete(List<Long> ids) {
		List<Comment> list = new ArrayList<>();
		ids.forEach(id ->{
			commentRepository.updateDelTime(id, new Date());
			list.add(commentRepository.getOne(id));
		});
		//List<Comment> list = commentRepository.removeByIdIn(ids);
		if (CollectionUtils.isNotEmpty(list)) {
			list.forEach(po -> {
				userEventService.identityComment(po.getAuthorId(), false);
			});
		}
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void delete(long id, long authorId) {
		Optional<Comment> optional = commentRepository.findById(id);
		if (optional.isPresent()) {
			Comment po = optional.get();
			// 判断文章是否属于当前登录用户
			Assert.isTrue(po.getAuthorId() == authorId, "认证失败");
			commentRepository.deleteById(id);

			userEventService.identityComment(authorId, false);
		}
	}

	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void deleteByPostId(long postId) {
		List<Comment> list = commentRepository.removeByPostId(postId);
		if (CollectionUtils.isNotEmpty(list)) {
			Set<Long> userIds = new HashSet<>();
			list.forEach(n -> userIds.add(n.getAuthorId()));
			userEventService.identityComment(userIds, false);
		}
	}

	/**
	 * 根据postId获取所有评论,并设置delTime为当前时间
	 * @param postId
	 */
	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void setDelTimeByPostId(long postId) {
		// 根据postId获取所有
		List<Comment> list = commentRepository.queryAllByPostId(postId);
		if (CollectionUtils.isNotEmpty(list)) {
			Set<Long> userIds = new HashSet<>();
			list.forEach(n -> {
				n.setDelTime(new Date());
				commentRepository.save(n);
				userIds.add(n.getAuthorId());
			}
			);
			userEventService.identityComment(userIds, false);
		}
	}

	@Override
	public long count() {
		return commentRepository.count();
	}

	@Override
	public long countByAuthorIdAndPostId(long authorId, long toId) {
		return commentRepository.countByAuthorIdAndPostId(authorId, toId);
	}

	/**
	 * 对评论点赞
	 * @param id 该评论的id
	 */
	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void favor(long userId, long id) {
		commentRepository.updateFavors(id, Consts.IDENTITY_STEP);
		favoriteService.addFavorComment(userId, id);
	}

	/**
	 * 取消赞
	 * @param id 该评论的id
	 */
	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void unfavor(long userId, long id) {
		commentRepository.updateFavors(id, Consts.DECREASE_STEP);
		favoriteService.deleteFavorComment(userId, id);
	}

	/**
	 * 审核
	 * @param ids
	 * @param checkText
	 */
	@Override
	@Transactional(rollbackFor = Throwable.class)
	public void check(List<Long> ids, String checkText) {
		ids.forEach(id ->{
			commentRepository.updateCheck(id, checkText);
		});
	}



}
