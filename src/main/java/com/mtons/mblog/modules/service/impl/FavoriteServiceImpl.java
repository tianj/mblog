package com.mtons.mblog.modules.service.impl;

import com.mtons.mblog.modules.data.FavoriteVO;
import com.mtons.mblog.modules.data.PostVO;
import com.mtons.mblog.modules.entity.Comment;
import com.mtons.mblog.modules.entity.FavoriteComment;
import com.mtons.mblog.modules.repository.CommentRepository;
import com.mtons.mblog.modules.repository.FavoriteCommentRepository;
import com.mtons.mblog.modules.repository.FavoriteRepository;
import com.mtons.mblog.base.utils.BeanMapUtils;
import com.mtons.mblog.modules.entity.Favorite;
import com.mtons.mblog.modules.service.CommentService;
import com.mtons.mblog.modules.service.FavoriteService;
import com.mtons.mblog.modules.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.*;

/**
 * @author langhsu on 2015/8/31.
 */
@Slf4j
@Service
@Transactional(readOnly = true)
public class FavoriteServiceImpl implements FavoriteService {
    @Autowired
    private FavoriteRepository favoriteRepository;
    @Autowired
    private FavoriteCommentRepository favoriteCommentRepository;
    @Autowired
    private PostService postService;
    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Page<FavoriteVO> pagingByUserId(Pageable pageable, long userId) {
        Page<Favorite> page = favoriteRepository.findAllByUserId(pageable, userId);
        List<FavoriteVO> rets = new ArrayList<>();
        Set<Long> postIds = new HashSet<>();
        for (Favorite po : page.getContent()) {
            rets.add(BeanMapUtils.copy(po));
            postIds.add(po.getPostId());
        }

        if (postIds.size() > 0) {
            Map<Long, PostVO> posts = postService.findMapByIds(postIds);

            for (FavoriteVO t : rets) {
                PostVO p = posts.get(t.getPostId());
                t.setPost(p);
            }
        }
        return new PageImpl<>(rets, pageable, page.getTotalElements());
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void add(long userId, long postId) {
        Favorite po = favoriteRepository.findByUserIdAndPostId(userId, postId);

        Assert.isNull(po, "您已经赞过此文章");

        // 如果没有喜欢过, 则添加记录
        po = new Favorite();
        po.setUserId(userId);
        po.setPostId(postId);
        po.setCreated(new Date());

        favoriteRepository.save(po);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void delete(long userId, long postId) {
        Favorite po = favoriteRepository.findByUserIdAndPostId(userId, postId);
        Assert.notNull(po, "还没有喜欢过此文章");
        favoriteRepository.delete(po);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void deleteByPostId(long postId) {
        int rows = favoriteRepository.deleteByPostId(postId);
        log.info("favoriteRepository delete {}", rows);
    }

    @Override
    public Integer getFavorState(long userId, long postId) {
        Favorite po = favoriteRepository.findByUserIdAndPostId(userId,postId);
        if(po == null){
            return 0;
        }
        return 1;
    }

    /**
     * 新增评论的点赞详情
     * @param userId
     * @param commentId
     */
    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void addFavorComment(long userId, long commentId) {
        // 根据userid+commentid 判断是否赞过该评论
        FavoriteComment fc = favoriteCommentRepository.findByUserIdAndCommentId(userId, commentId);
        Assert.isNull(fc, "您已经赞过此评论");
        // 如果没有喜欢过, 则添加记录
        fc = new FavoriteComment();
        Optional<Comment> c =  commentRepository.findById(commentId);
        if(c.get() != null){
            fc.setPostId(c.get().getPostId());
        }
        fc.setUserId(userId);
        fc.setCommentId(commentId);
        fc.setCreated(new Date());
        favoriteCommentRepository.save(fc);
    }

    /**
     * 删除评论的点赞详情
     * @param userId
     * @param commentId
     */
    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void deleteFavorComment(long userId, long commentId) {
        FavoriteComment fc = favoriteCommentRepository.findByUserIdAndCommentId(userId, commentId);
        Assert.notNull(fc, "还没有赞过此评论");
        favoriteCommentRepository.delete(fc);
    }

}
