/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package com.mtons.mblog.web.controller.admin;

import java.util.List;

import com.mtons.mblog.base.lang.Consts;
import com.mtons.mblog.base.lang.Result;
import com.mtons.mblog.modules.entity.Comment;
import com.mtons.mblog.modules.event.MessageEvent;
import com.mtons.mblog.web.controller.BaseController;
import com.mtons.mblog.modules.data.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtons.mblog.modules.service.CommentService;

/**
 * @author langhsu
 *
 */
@Controller("adminCommentController")
@RequestMapping("/admin/comment")
public class CommentController extends BaseController {
	@Autowired
	private CommentService commentService;

	@Autowired
	private ApplicationContext applicationContext;
	
	@RequestMapping("/list")
	public String list(ModelMap model) {
		// Pageable pageable = wrapPageable();
		Pageable pageable = wrapPageable(Sort.by(Sort.Direction.DESC, "created"));
		Page<CommentVO> page = commentService.paging4Admin(pageable);
		model.put("page", page);
		return "/admin/comment/list";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestParam("id") List<Long> id) {
		Result data = Result.failure("操作失败");
		if (id != null) {
			try {
				commentService.delete(id);
				data = Result.success();
			} catch (Exception e) {
				data = Result.failure(e.getMessage());
			}
		}
		return data;
	}

	@RequestMapping("/checkText")
	@ResponseBody
	public Result check(@RequestParam("id") List<Long> id,
						@RequestParam(value = "checkText",defaultValue = "审核中") String checkText) {
		Result data = Result.failure("操作失败");
		if (id != null) {
			try {
				commentService.check(id, checkText);
				id.forEach(i ->{
					if("审核通过".equals(checkText)){
						Comment co = commentService.findById(i);
						sendMessage(co.getAuthorId(), co.getPostId(), co.getPid());
					}
				});
				data = Result.success();
			} catch (Exception e) {
				data = Result.failure(e.getMessage());
			}
		}
		return data;
	}

	/**
	 * 发送通知
	 * @param userId
	 * @param postId
	 */
	private void sendMessage(long userId, long postId, long pid) {
		MessageEvent event = new MessageEvent("MessageEvent");
		event.setFromUserId(userId);
		if (pid > 0) {
			event.setEvent(Consts.MESSAGE_EVENT_COMMENT_REPLY);
			event.setCommentParentId(pid);
		} else {
			event.setEvent(Consts.MESSAGE_EVENT_COMMENT);
		}
		// 此处不知道文章作者, 让通知事件系统补全
		event.setPostId(postId);
		applicationContext.publishEvent(event);
	}

}
